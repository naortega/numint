cmake_minimum_required(VERSION 2.8)
project(NumericalIntegration)

# Define target
set(TARGET_NAME numint)

# Use debug if none other specified
if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Debug)
endif()

# Define sources
set(SRCS
	src/main.c)

# Define the C flags.
set(CMAKE_C_FLAGS "-std=gnu99 -Wall -Wextra -Werror -Wfatal-errors -Wmissing-declarations -pedantic-errors")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS} -g -O0")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS} -O3")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS} -g -O3")
set(CMAKE_C_FLAGS_MINSIZEREL "${CMAKE_C_FLAGS} -Os")

set(CMAKE_EXE_LINKER_FLAGS "-lm")

# Create the executable
add_executable(${TARGET_NAME} ${SRCS})
