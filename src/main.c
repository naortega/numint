/**
 * Copyright (c) 2016 Nicolás A. Ortega
 * License: GNU GPLv3
 */
#include <stdio.h>
#include <math.h>

inline double func(double x);

int main(void) {
	double a, b;
	printf("Enter \"min, max\": ");
	scanf("%lf, %lf", &a, &b);

	if(a > b) {
		fprintf(stderr, "`min' cannot be larger than `max'.\n");
		return 1;
	}

	unsigned int parts;
	printf("Number of parts: ");
	scanf("%u", &parts);

	if(parts == 0) {
		fprintf(stderr, "`parts' must at least be 1.\n");
		return 1;
	}

	double lRect, rRect, mRect, trap, simp;
	lRect = rRect = mRect = trap = simp = 0;
	for(unsigned int i = 0; i < parts; ++i) {
		double p0 = a + (i * (b - a) / parts);
		double p1 = p0 + ((b - a) / parts);
		lRect += func(p0) * (p1 - p0);
		rRect += func(p1) * (p1 - p0);
		mRect += func((p0 + p1) / 2) * (p1 - p0);
		trap += (func(p0) + func(p1)) / 2 * (p1 - p0);
		simp += (p1 - p0) / 6 * (func(p0) + func(p1) + 4 * func((p0 + p1) / 2));
	}

	printf("Left Rectangle: %lf\n", lRect);
	printf("Right rectangle: %lf\n", rRect);
	printf("Middle Point Rectangle: %lf\n", mRect);
	printf("Trapezoidal: %lf\n", trap);
	printf("Simpson: %lf\n", simp);

	return 0;
}

double func(double x) {
	return pow(x, 3.0) + 4.0 * x - 2.0;
}
