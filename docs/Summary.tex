%%%%%
%
% Copyright (c) 2016 Nicolás A. Ortega
% This document is licensed under a Creative Commons
% Attribution-Share Alike 4.0 International License.
%
%%%%%
\documentclass[12pt, a4paper]{article}
\usepackage{fontspec}
% Set language of document
\usepackage[english]{babel}
\usepackage{hyperref}

\begin{document}
\title{\texttt{numint}}
\author{Nicolás A. Ortega}
\maketitle
\section{Usage}
\texttt{numint} is a quick program written in C that calculates the area underneath a function using Left Rectangle, Right Rectangle, Middle-Point Rectangle, Trapezoidal, and Simpson part integrations. The usage is simple, first define the function you wish to integrate in the \texttt{`func'} function found at the end of the \texttt{`main.c'} file. After doing so compile and run the program. By default \texttt{numint} uses the CMake build system, therefore with CMake installed you could run the following commands from the root directory of the project:

\begin{verbatim}
$ cd build/
$ cmake ..
$ make
\end{verbatim}

This would generate a file, \texttt{`build/numint'} which is the program's executable file. On UNIX systems you can run this file directly from the command-line from the \texttt{`build/'} directory by running \texttt{`./numint'}. At this point \texttt{numint} will prompt you with \texttt{`Enter "min, max":'}, which is asking you to enter the range in which you want to calculate the integral (e.g. \texttt{`4, 6'}), these numbers may contain decimals. After this you will be prompted \texttt{`Number of parts:'} which is asking for the number parts for the range previously given should be divided into (this number \emph{cannot} contain a decimal).

If all has been done correctly then the program should print the results of the integrations for the formula in all five methods.

\section{Examples}
In order make sure that the program works the way we think it does we'd have to test it first. For these examples let's use the integral $\int_{2}^{5} x^{3} + 4x - 2$. The results taken by hand for this integral in one part for all forms are as follows:
\begin{itemize}
	\item Left Rectangle: $f(a) \cdot (b - a) = (2^{3} + 4 \cdot 2 - 2) \cdot (5 - 2) = 42$
	\item Right Rectangle: $f(b) \cdot (b - a) = (5^{3} + 4 \cdot 5 - 2) \cdot (5 - 2) = 429$
	\item Middle-Point Rectangle: $f(\frac{a + b}{2}) \cdot (b - a) = (3.5^{3} + 4 \cdot 3.5 - 2) \cdot (5 - 2) = 164.625$
	\item Trapezoid: $\frac{f(a) + f(b)}{2} \cdot (b - a) = \frac{(2^{3} + 4 \cdot 2 - 2) + (5^{3} + 4 \cdot 5 - 2)}{2} \cdot (5 - 2) = 235.5$
	\item Simpson: $\frac{b - a}{6} \cdot (f(a) + f(b) + 4 \cdot f(\frac{a + b}{2})) = \frac{5 - 2}{6} \cdot ((2^{3} + 4 \cdot 2 - 2) + (5^{3} + 4 \cdot 5 - 2) + 4 \cdot (3.5^{3} + 4 \cdot 3.5 - 2)) = 188.25$
\end{itemize}

If we run this in \texttt{numint} we get the following output:
\begin{verbatim}
Enter "min, max": 2, 5
Number of parts: 1
Left Rectangle: 42.000000
Right rectangle: 429.000000
Middle Point Rectangle: 164.625000
Trapezoidal: 235.500000
Simpson: 188.250000
\end{verbatim}

As you can see you get the exact same results (with trailing zeros due to the \texttt{`double'} data type).

We have now seen how \texttt{numint} works for integrals in one part, but we should also test it for multiple parts. Let's use the same integral but test for 2 parts, which should give us slightly more accuracy. The results are as follows:

\begin{itemize}
	\item Left Rectangle: $\displaystyle\sum_{i = 1}^{2} f(x_{i}) \cdot (x_{i + 1} - x_{i}) = 103.3125$
	\item Right Rectangle: $\displaystyle\sum_{i = 1}^{2} f(x_{i + 1}) \cdot (x_{i + 1} - x_{i}) = 296.8125$
	\item Middle-Point Rectangle: $\displaystyle\sum_{i = 1}^{2} f(\frac{x_{i} + x_{i + 1}}{2}) \cdot (x_{i + 1} - x_{i}) = 182.34375$
	\item Trapezoidal: $\displaystyle\sum_{i = 1}^{2} \frac{f(x_{i}) + f(x_{i + 1})}{2} \cdot (x_{i + 1} - x_{i}) = 200.0625$
	\item Simpson: $\displaystyle\sum_{i = 1}^{2} \frac{x_{i + 1} - x_{i}}{6} \cdot (f(x_{i}) + f(x_{i + 1}) + 4 \cdot f(\frac{x_{i} + x_{i + 1}}{2})) = 188.25$
\end{itemize}

If we then run this in \texttt{numint} we get the following output:
\begin{verbatim}
Enter "min, max": 2, 5
Number of parts: 2
Left Rectangle: 103.312500
Right rectangle: 296.812500
Middle Point Rectangle: 182.343750
Trapezoidal: 200.062500
Simpson: 188.250000
\end{verbatim}

Again the same exact results.

\section{Technical Analysis}
Now let's analyze how the code works. To begin let's look at the function $f(x)$ which is defined in the \texttt{`func(double x)'} C function. The function is defined as follows:
\begin{verbatim}
double func(double x) {
    return pow(x, 3.0) + 4.0 * x - 2.0;
}
\end{verbatim}

It's a simple function that takes in \texttt{`double'} as a parameter and returns the result in the form of a \texttt{`double'} as well. The function itself uses the \texttt{`pow()'} from the C Math library which is what allows us to easily use exponents (in this case \texttt{`pow(x, 3.0)'} is equivalent to $x^{3}$). If you want to change the function you'll have to modify the \texttt{`return'} statement in this function.

Later, after retrieving the \texttt{`min'}, \texttt{`max'}, and \texttt{`parts'} variables that were prompted to the user we enter the main loop of the program for the calculations.
\begin{verbatim}
for(unsigned int i = 0; i < parts; ++i) {
    ...
}
\end{verbatim}

This loop will create a variable \texttt{`i'} which will be useful later. This variable (as stated by the loop) will increment while it is less than the \texttt{`parts'} variable.

After this are the following lines:
\begin{verbatim}
double p0 = a + (i * (b - a) / parts);
double p1 = p0 + ((b - a) / parts);
\end{verbatim}

Here we create two variables: \texttt{`p0'} and \texttt{`p1'}. In regards to the previous calculations done in the `Examples' section, \texttt{`p0'} and \texttt{`p1'} would be equivalent to $x_{i}$ and $x_{i + 1}$ respectively. So, let's imagine that we're looking at the first part of two for this integral that goes from 2 to 5 (like in the example). In this case, mathematically, the equation would look like this: $p0 = 2 + \frac{0 \cdot (5 - 2)}{2}$ and $p1 = p0 + \frac{5 - 2}{2}$. Now, why multiply by 0? Doesn't the loop start at 1? Well, in programming, luckily, we always start with 0 (remember in the \texttt{`for'} loop where it said \texttt{`unsigned int i = 0'}?). Since this is the first part we multiply by 0 because we want to start at 2. This is all being setup for the calculation of the integrals with parts.

Finally comes the calculation of the integrals, which is as follows:
\begin{verbatim}
lRect += func(p0) * (p1 - p0);
rRect += func(p1) * (p1 - p0);
mRect += func((p0 + p1) / 2) * (p1 - p0);
trap += (func(p0) + func(p1)) / 2 * (p1 - p0);
simp += (p1 - p0) / 6 * (func(p0) + func(p1) + 4 * func((p0 + p1) / 2));
\end{verbatim}

All these variables (\texttt{`lRect'}, \texttt{`rRect'}, etc.) have all been assigned to 0 before entering the loop. Therefore, on each of these lines the integral for a given part of the function (where \texttt{`p0'} is the beginning of the part and \texttt{`p1'} is the end of the part) which is then added to all the other parts because of the \texttt{`for'} loop seen previously.

After this all the variables are printed to \texttt{`stdout'} (Standard Output) for the user to see the results.

\section{Modifications}
Currently \texttt{numint} is limited to a pre-coded function (as previously mentioned, this function can be modified in \texttt{`func(double x)'} found at the end of the file \texttt{`main.c'}). Modifications to the source for this project are permitted and encouraged, however all distributions of said changes must comply with the project license, which is the GNU General Public License version 3.

\section{License}
This document is licensed under a Creative Commons Attribution-Share Alike 4.0 International License.

Copyright \copyright{} 2016 Nicolás A. Ortega \texttt{<\href{mailto:deathsbreed@themusicinnoise.net}{deathsbreed@themusicinnoise.net}>}

\end{document}
