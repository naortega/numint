Numeric Integration
===================
[![build status](https://gitlab.com/Deathsbreed/numint/badges/master/build.svg)](https://gitlab.com/Deathsbreed/numint/commits/master)

Calculates the integration of a formula by parts (formula defined in the `func()` function).

Building
--------
Make sure you have CMake installed and run the following:
```bash
$ cd build/
$ cmake ..
$ make
```
There should be a binary called numint.

License
-------
This program is licensed with the [GNU GPLv3](/LICENSE).
